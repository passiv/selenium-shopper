package sk.johan.alza;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import sk.johan.AbstractPageComponent;
import sk.johan.SearchCondition;
import sk.johan.util.SearchResult;
import sk.johan.util.Util;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AlzaStore extends AbstractPageComponent {

    public AlzaStore(WebDriver webDriver) {
        super(webDriver, "http://www.alza.sk",
                "/graficke-karty-s-cipom-nvidia/18849879.htm#f&cst=null&cud=0&pg=1-100&prod=&par340=340-239785900,340-239785898,340-239785899,340-239797673,340-239807474&sc=19870");
    }

    @Override
    public synchronized List<SearchResult> search(Map<String, SearchResult> allSearchResults) {
        List<SearchResult> result = new LinkedList<>();

        waitUntilSpinnersDisappear();

        WebElement productGrid = webDriver.findElement(By.className("browsingitemcontainer"));

        loadAllProducts();

        List<WebElement> productInfos = productGrid.findElements(By.className("canBuy"));
        for (WebElement productInfo : productInfos) {
            abstractPageTest.fastScrollToElement(productInfo);

            markElementBeingSearched(productInfo);

            WebElement priceElement = productInfo.findElement(By.className("c2"));
            if (priceElement != null && StringUtils.isNotBlank(priceElement.getText())) {
                WebElement productTitle = productInfo.findElement(By.className("name"));

                String priceString = priceElement.getText();
                priceString = priceString.replace(" ", "");
                priceString = priceString.replace("€", "");
                priceString = priceString.replace("Od", "");
                priceString = priceString.replace(",", ".");

                Double price = Double.parseDouble(priceString);

                for (SearchCondition searchCondition : this.getSearchConditions()) {
                    if (searchCondition.isMatch(productTitle.getText(), price)) {
                        String searchHitLink = productTitle.getAttribute("href");

                        if (allSearchResults.get(searchHitLink) == null) {
                            SearchResult searchResult = new SearchResult(searchCondition.getSearchPhrase(), price, searchHitLink);
                            result.add(searchResult);
                            System.out.println(Util.formatDateTime(LocalDateTime.now()) + " | " + result.toString());
                            playSound("src/main/resources/notification.wav");
                            Util.sendEmail(searchCondition.getSearchPhrase() + " at " + price, searchResult.toString());
                        }
                    }
                }
            }
        }
        return result;
    }

    private void waitUntilSpinnersDisappear() {
        List<WebElement> spinners = webDriver.findElements(By.className("circle-loader-container"));

        for (WebElement spinner : spinners) {
            if (spinner.isDisplayed()) {
                try {
                    this.abstractPageTest.doWait().until(ExpectedConditions.invisibilityOf(spinner));
                    Thread.sleep(500);
                    waitUntilSpinnersDisappear();
                } catch (Exception e) {
                    // do nothing
                }
            }
        }
    }

    protected void loadAllProducts() {
        boolean continueScroll = true;
        while (continueScroll) {
            abstractPageTest.fastScrollDownByPixels(1000);
            waitUntilSpinnersDisappear();

            List<WebElement> footers = webDriver.findElements(By.id("footer"));
            if (!footers.isEmpty()) {
                continueScroll = !abstractPageTest.isVisibleInViewport(footers.get(0));
            }
        }
    }

}