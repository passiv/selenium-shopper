/*
 * Copyright 2016-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sk.johan;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Abstract class to create utility methods to access WebClient and Page.
 */
public class AbstractPageTest {
    private static String TEST_BROWSER = null;

    static {
        if (System.getenv("seleniumTestBrowser") != null) {
            TEST_BROWSER = System.getenv("seleniumTestBrowser");
        }
    }

    private long port;

    private static WebDriver webDriver;

    private static int countFinish = 0;

    public static void init() {
        if (webDriver == null) {
            if (TEST_BROWSER == null) {
                webDriver = getChromeDriver();
            } else {
                switch (TEST_BROWSER) {
                    case "htmlunit" :
                        webDriver = getHtmlUnitDriver();
                        break;
                    case "firefox":
                        webDriver = getFirefoxDriver();
                        break;
                    case "chrome":
                        webDriver = getChromeDriver();
                        break;
                    case "chrome_headless":
                        webDriver = getHeadlessChromeDriver();
                        break;
                    default:
                        webDriver = getChromeDriver();
                        break;
                }
            }
        }
    }

    private static WebDriver getHtmlUnitDriver() {
        return new HtmlUnitDriver(true);
    }

    private static WebDriver getChromeDriver() {
        WebDriverManager.getInstance(ChromeDriver.class).setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");//fullscreen
        return new ChromeDriver(options);
    }

    private static WebDriver getHeadlessChromeDriver() {
        WebDriverManager.getInstance(ChromeDriver.class).setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        return new ChromeDriver(options);
    }

    private static WebDriver getFirefoxDriver() {
        WebDriverManager.getInstance(FirefoxDriver.class).setup();
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        // setting preference because of https://github.com/mozilla/geckodriver/issues/659
        firefoxProfile.setPreference("dom.file.createInChild", true);
        firefoxOptions.setProfile(firefoxProfile);
        return new FirefoxDriver(firefoxOptions);
    }

    public static void finish() {
        webDriver.close();
    }

    public static WebDriver getWebDriver() {
        if (webDriver == null) {
            init();
        }
        return webDriver;
    }

    protected <T extends AbstractPageComponent> T initElements(Class<T> classx, List<SearchCondition> searchPhrases) {
        T result = PageFactory.initElements(webDriver, classx);
        result.setPrefix(result.getPrefix());
        result.setSearchConditions(searchPhrases);
        result.setAbstractPageTest(this);
        return result;
    }

    protected void doLogin() {
        doLogin("admin", "admin");
    }

    protected void doLogin(String username, String password) {
//        if (!"TEA".equals(webDriver.getTitle())) { //if is not logged in
//            LoginPage loginPage = initElements(LoginPage.class);
//            loginPage.navigateTo();
//
//            OverviewPage overviewPage = loginPage.login(username, password);
//            assertThat(overviewPage.hasPageRendered());
//        }
    }

    public void scrollDownToElement(String elementId) {
        WebElement element = getWebDriver().findElement(By.id(elementId));
        scrollToElement(element, 500);
    }

    public void scrollDownToElementWithClass(String clazz) {
        WebElement element = getWebDriver().findElement(By.className(clazz));
        scrollToElement(element, 500);
    }

    public void scrollDownToElement(WebElement element) {
        ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].scrollIntoView({behavior: 'smooth',\n" +
                "            block: 'center',\n" +
                "            inline: 'center'});", element);
    }

    public void scrollToElement(WebElement element, int sleepInMillis) {
        scrollDownToElement(element);
        threadSleep(sleepInMillis);
    }

    public void fastScrollToElement(WebElement element) {
        ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].scrollIntoView({behavior: 'auto',\n" +
                "            block: 'center',\n" +
                "            inline: 'center'});", element);
    }

    public void threadSleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException("Could not sleep", e);
        }
    }

    public void scrollDownByPixels(int pixels) {
        ((JavascriptExecutor) getWebDriver()).executeScript("window.scrollBy({\n" +
                "    top: arguments[0],\n" +
                "    left: 0,\n" +
                "    behavior : \"smooth\"\n" +
                "})\n", pixels);
    }

    public void fastScrollDownByPixels(int pixels) {
        ((JavascriptExecutor) getWebDriver()).executeScript("window.scrollBy({\n" +
                "    top: arguments[0],\n" +
                "    left: 0,\n" +
                "    behavior : \"auto\"\n" +
                "})\n", pixels);
    }

    protected void sendKeys(String elementId, String keys) {
        doSleep(1000);
        WebElement element = getWebDriver().findElement(By.cssSelector("[id*='" + elementId + "']"));
        element.clear();
        element.sendKeys(keys);
        doSleep(1000);
    }

    public WebElement moveMouseCursor(By elementPath) {
        WebElement element = getWebDriver().findElement(elementPath);

        return moveMouseCursor(element);
    }

    public WebElement moveMouseCursor(WebElement element) {
        Actions action = new Actions(getWebDriver());

        if (element == null) {
            return null;
        }

        action.moveToElement(element);
        action.perform();

        return element;
    }

    public void clickMouse() {
        Actions action = new Actions(getWebDriver());
        action.click();
        action.perform();
    }

    public WebDriverWait doSleep(long sleepInMillis) {
        return new WebDriverWait(getWebDriver(), 3, sleepInMillis);
    }

    public WebDriverWait doWait() {
        return new WebDriverWait(getWebDriver(), 3);
    }

    public Boolean isVisibleInViewport(WebElement element) {
        WebDriver driver = ((RemoteWebElement)element).getWrappedDriver();

        return (Boolean)((JavascriptExecutor)driver).executeScript(
                "var elem = arguments[0],                 " +
                        "  box = elem.getBoundingClientRect(),    " +
                        "  cx = box.left + box.width / 2,         " +
                        "  cy = box.top + box.height / 2,         " +
                        "  e = document.elementFromPoint(cx, cy); " +
                        "for (; e; e = e.parentElement) {         " +
                        "  if (e === elem)                        " +
                        "    return true;                         " +
                        "}                                        " +
                        "return false;                            "
                , element);
    }

}