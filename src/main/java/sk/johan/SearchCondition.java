package sk.johan;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

@Getter
@Setter
public class SearchCondition {

    private String searchPhrase;
    private Double minPrice, maxPrice;

    public SearchCondition(String searchPhrase, Double minPrice, Double maxPrice) {
        this.searchPhrase = searchPhrase;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }

    public boolean isMatch(String productTitle, Double price) {
        if (!StringUtils.isBlank(productTitle) && price != null) {
            if (productTitle.toLowerCase().contains(searchPhrase.toLowerCase())
                    && price >= minPrice && price <= maxPrice) {
                return true;
            }
        }
        return false;
    }
}