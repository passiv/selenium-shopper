package sk.johan.amazon;

import org.openqa.selenium.WebDriver;

public class MsiStore extends AmazonPage {

    public MsiStore(WebDriver webDriver) {
        super(webDriver, "/stores/page/34DBEB29-3562-4CF1-A97A-4EAA377F5139?ingress=2&visitId=f295e3ed-bf58-4e62-8a5e-60bec71e9939&ref_=ast_bln");
    }
}