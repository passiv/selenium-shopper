package sk.johan.amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EvgaStore extends AmazonPage {

    public EvgaStore(WebDriver webDriver) {
        super(webDriver, "/stores/page/33431CCD-44BE-4FE2-A2BE-F8BBA8360DAC?ingress=2&visitId=53045a11-1dc2-4a5b-8921-21d4759df4bb&ref_=ast_bln");
    }

    @Override
    protected void loadAllProducts(WebElement productGrid, WebElement footer) {
        this.abstractPageTest.threadSleep(500);
    }

}