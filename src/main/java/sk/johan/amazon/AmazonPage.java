package sk.johan.amazon;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import sk.johan.AbstractPageComponent;
import sk.johan.SearchCondition;
import sk.johan.util.SearchResult;
import sk.johan.util.Util;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AmazonPage extends AbstractPageComponent {

    public AmazonPage(WebDriver webDriver, String url) {
        super(webDriver, "http://www.amazon.com", url);
    }

    public AmazonPage(WebDriver webDriver, String site, String url) {
        super(webDriver, site, url);
    }

    @Override
    public synchronized List<SearchResult> search(Map<String, SearchResult> allSearchResults) {
        if (!webDriver.getTitle().contains("Amazon") && !webDriver.getTitle().contains("GPUTracker")) {
            webDriver.close();
        }

        List<SearchResult> result = new LinkedList<>();
        abstractPageTest.scrollDownToElementWithClass("navFooterLine");

        waitUntilSpinnersDisappear();
        WebElement productGrid = webDriver.findElement(By.cssSelector("div[data-widgettype='ProductGrid']"));

        WebElement footer = this.findElementById("navBackToTop");

        loadAllProducts(productGrid, footer);

        List<WebElement> productInfos = productGrid.findElements(By.className("ProductGridItem__itemInfoChild__1HpO6"));

        for (WebElement productInfo : productInfos) {
            abstractPageTest.fastScrollToElement(productInfo);
            markElementBeingSearched(productInfo);
            WebElement productTitle = productInfo.findElement(By.className("ProductGridItem__title__2C1kS"));

            WebElement priceElement = null;

            if (!productInfo.findElements(By.className("style__whole__3EZEk")).isEmpty()) {
                priceElement = productInfo.findElement(By.className("style__whole__3EZEk"));
            }

            if (priceElement != null && StringUtils.isNotBlank(priceElement.getText())) {
                Double price = Double.parseDouble(priceElement.getText().replace(",", ""));
                for (SearchCondition searchCondition : this.getSearchConditions()) {
                    if (searchCondition.isMatch(productTitle.getText(), price)) {
                        String searchHitLink = productTitle.getAttribute("href");
                        if (allSearchResults.get(searchHitLink) == null) {
                            SearchResult searchResult = new SearchResult(searchCondition.getSearchPhrase(), price, searchHitLink);
                            result.add(searchResult);
                            System.out.println(Util.formatDateTime(LocalDateTime.now()) + " | " + result.toString());
                            playSound("src/main/resources/notification.wav");
                            Util.sendEmail(searchCondition.getSearchPhrase() + " at " + price, searchResult.toString());
                        }
                    }
                }
            }
        }
        return result;
    }

    private void waitUntilSpinnersDisappear() {
        List<WebElement> spinners = webDriver.findElements(By.className("a-spinner"));
        if (!spinners.isEmpty()) {
            try {
                this.abstractPageTest.doWait().until(ExpectedConditions.invisibilityOf(spinners.get(0)));
            } catch (Exception e) {
                // do nothing
            }
        }
    }

    protected void loadAllProducts(WebElement productGrid, WebElement footer) {
        boolean continueScroll = true;
        while (continueScroll) {
            abstractPageTest.scrollDownByPixels(500);
            waitUntilSpinnersDisappear();

            clickShowMoreButtons(productGrid);
            continueScroll = !abstractPageTest.isVisibleInViewport(footer);
        }
    }

    private void clickShowMoreButtons(WebElement productGrid) {
        this.abstractPageTest.threadSleep(1000);
        WebElement showMore = getShowMoreButton(productGrid);

        if (showMore != null) {
            abstractPageTest.scrollDownToElement(showMore);
            try {
                if (getShowMoreButton(productGrid) != null) {
                    WebElement showMoreButton = getShowMoreButton(productGrid);
                    showMoreButton.click();
                    this.abstractPageTest.threadSleep(500);
                    clickShowMoreButtons(productGrid);
                }
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    }

    private WebElement getShowMoreButton(WebElement productGrid) {
        WebElement showMore;

        try {
            showMore = productGrid.findElement(By.xpath("//*[text()='Show more']"));
        } catch(NoSuchElementException e) {
            showMore = null;
        }
        return showMore;
    }

}