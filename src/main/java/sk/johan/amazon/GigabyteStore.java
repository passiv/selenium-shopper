package sk.johan.amazon;

import org.openqa.selenium.WebDriver;

public class GigabyteStore extends AmazonPage {

    public GigabyteStore(WebDriver webDriver) {
        super(webDriver, "/stores/page/449FD61C-E4DA-4494-8CFD-ED0F518E379B?ingress=2&visitId=0e380f3c-e829-4f27-9c75-3960065c522a&ref_=ast_bln&productGridPageIndex=2");
    }

}