package sk.johan.amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PnyDEStore extends AmazonPage{

    public PnyDEStore(WebDriver webDriver) {
        super(webDriver, "http://www.amazon.de", "/stores/page/254CDAE2-51B7-411E-92F8-7991907B9C7F?ingress=2&visitId=d81c9939-953e-40f9-a103-90b563f29433&ref_=bl_dp_s_web_7091775031");
    }

    @Override
    protected void loadAllProducts(WebElement productGrid, WebElement footer) {
        //DO NOTHING
    }
}