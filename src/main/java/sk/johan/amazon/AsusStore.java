package sk.johan.amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AsusStore extends AmazonPage {

    public AsusStore(WebDriver webDriver) {
        super(webDriver, "/stores/page/1823690D-D514-44A6-8FA3-7E62169A32F9?ingress=2&visitId=6a217318-338f-4836-959e-73a002a5adfb&ref_=ast_bln");
    }

    @Override
    protected void loadAllProducts(WebElement productGrid, WebElement footer) {
        this.abstractPageTest.threadSleep(3000);
    }
}