package sk.johan.amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PnyStore extends AmazonPage{

    public PnyStore(WebDriver webDriver) {
        super(webDriver, "/stores/page/03F72C1A-1DFC-4993-BE67-E70A08AC6DD7?ingress=2&visitId=808f658f-2d47-4c3d-8cf4-979d51c81f5c&ref_=ast_bln");
    }

    @Override
    protected void loadAllProducts(WebElement productGrid, WebElement footer) {
        this.abstractPageTest.threadSleep(500);
    }

}