package sk.johan;

import lombok.SneakyThrows;
import org.eclipse.jetty.websocket.api.Session;
import sk.johan.alza.AlzaStore;
import sk.johan.amazon.*;
import sk.johan.gputracker.GputrackerStore;
import sk.johan.shops.LdlcStore;
import sk.johan.util.SearchResult;
import sk.johan.util.TimeRange;
import sk.johan.util.Util;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.*;
import java.net.PasswordAuthentication;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Application {

    private static List<SearchCondition> gpuSearchPhrases = new LinkedList<SearchCondition>();

    public static Properties properties;

    public static void main(String[] args) {
        System.out.println("hello");

        loadApplicationProperties();
        AbstractPageTest abstractPageTest = new AbstractPageTest();

        abstractPageTest.init();

        List<AbstractPageComponent> pages = new LinkedList<>();
        pages.add(abstractPageTest.initElements(GputrackerStore.class, gpuSearchPhrases));
//        pages.add(abstractPageTest.initElements(LdlcStore.class, gpuSearchPhrases));
        pages.add(abstractPageTest.initElements(AlzaStore.class, gpuSearchPhrases));

        pages.add(abstractPageTest.initElements(GigabyteStore.class, gpuSearchPhrases));
        pages.add(abstractPageTest.initElements(MsiStore.class, gpuSearchPhrases));
        pages.add(abstractPageTest.initElements(AsusStore.class, gpuSearchPhrases));
        pages.add(abstractPageTest.initElements(PnyStore.class, gpuSearchPhrases));
        pages.add(abstractPageTest.initElements(PnyDEStore.class, gpuSearchPhrases));

        boolean proceed = true;
        Long cycleNumber = 0L;

        Map<String, SearchResult> allSearchResults = new HashMap<>();
        while (proceed) {
            clearScreen();
            cycleNumber++;
            LocalDateTime start = LocalDateTime.now();

            try {
                for (AbstractPageComponent page : pages) {
                    page.navigateTo();

                    List<SearchResult> searchResults = page.search(allSearchResults);

                    for (SearchResult result : searchResults) {
                        allSearchResults.put(result.getLink(), result);
                    }
                }
                LocalDateTime end = LocalDateTime.now();
                long duration = ChronoUnit.SECONDS.between(start, end);
                System.out.println(Util.formatDateTime(LocalDateTime.now()) + "| cycle number: " + cycleNumber + " | " + duration + "s");

            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }
        }
        AbstractPageTest.finish();
    }

    private static void printSearchHits(Collection<SearchResult> searchHits) {
        for (SearchResult s : searchHits) {
            System.out.println(s.toString());
        }
    }

    private static void loadApplicationProperties() {
        try {
            File propertiesFile = new File("src/main/resources/application.properties");
            InputStream targetStream = new FileInputStream(propertiesFile);
            properties = new Properties();
            properties.load(targetStream);

            properties.forEach((o, o2) -> {
                String[] property = ((String) o).split("\\.");
                String phrase = property[0];
                String priceProperty = property[1];

                boolean propertyIsLoaded = false;
                for (SearchCondition s : gpuSearchPhrases) {
                    if (s.getSearchPhrase().equalsIgnoreCase(phrase)) {
                        propertyIsLoaded = true;
                        if ("price-min".equalsIgnoreCase(priceProperty)) {
                            s.setMinPrice(Double.parseDouble((String) o2));
                        }

                        if ("price-max".equalsIgnoreCase(priceProperty)) {
                            s.setMaxPrice(Double.parseDouble((String) o2));
                        }
                    }
                }

                if (!propertyIsLoaded) {
                    SearchCondition searchCondition = new SearchCondition(phrase, 0.0, 0.0);
                    if ("price-min".equalsIgnoreCase(priceProperty)) {
                        searchCondition.setMinPrice(Double.parseDouble((String) o2));
                    }

                    if ("price-max".equalsIgnoreCase(priceProperty)) {
                        searchCondition.setMaxPrice(Double.parseDouble((String) o2));
                    }

                    gpuSearchPhrases.add(searchCondition);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("Could not load application.properties file", e);
        }
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}