package sk.johan.gputracker;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import sk.johan.AbstractPageComponent;
import sk.johan.SearchCondition;
import sk.johan.util.SearchResult;
import sk.johan.util.Util;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GputrackerStore extends AbstractPageComponent {

    public GputrackerStore(WebDriver webDriver) {
        super(webDriver, "https://www.gputracker.eu", "/en/search/category/1/graphics-cards?textualSearch=rtx&onlyInStock=true");
    }

    @Override
    public synchronized List<SearchResult> search(Map<String, SearchResult> allSearchResults) {
        if (!webDriver.getTitle().contains("Amazon") && !webDriver.getTitle().contains("GPUTracker")) {
            webDriver.close();
        }

        List<SearchResult> result = new LinkedList<>();

        WebElement productGrid = webDriver.findElement(By.id("facet-search-results"));
        loadAllProducts(productGrid);

        List<WebElement> products = productGrid.findElements(By.className("stock-item-block"));
        for (WebElement product : products) {
            abstractPageTest.fastScrollToElement(product);
            markElementBeingSearched(product);
            List<WebElement> itemNames = product.findElements(By.className("item-name"));
            String itemName = null;
            if (!itemNames.isEmpty()) {
                WebElement itemNameElement = itemNames.get(0);
                itemName = itemNameElement.getText();
            }

            Double price = getPrice(product);

            WebElement productLink = product.findElement(By.className("tracked-product-click"));
            for (SearchCondition searchCondition : this.getSearchConditions()) {
                if (searchCondition.isMatch(itemName, price)) {
                    String searchHitLink = productLink.getAttribute("href");
                    if (allSearchResults.get(searchHitLink) == null) {
                        SearchResult searchResult = new SearchResult(searchCondition.getSearchPhrase(), price, searchHitLink);
                        result.add(searchResult);
                        System.out.println(Util.formatDateTime(LocalDateTime.now()) + " | " + result.toString());
                        playSound("src/main/resources/notification.wav");
                        Util.sendEmail(searchCondition.getSearchPhrase() + " at " + price, searchResult.toString());
                    }
                }
            }

        }
        return result;
    }

    protected void loadAllProducts(WebElement productGrid) {
        List<WebElement> loadMoreButtons = productGrid.findElements(By.className("searchEngineLoadMoreButton"));

        if (!loadMoreButtons.isEmpty()) {
            WebElement loadMoreButton = loadMoreButtons.get(0);

            this.abstractPageTest.scrollDownToElement(loadMoreButton);
            if ("Load more".equals(loadMoreButton.getText())) {
                loadMoreButton.click();
                this.abstractPageTest.doSleep(100);
            }
        }
        if (!productGrid.findElements(By.className("searchEngineLoadMoreButton")).isEmpty() && !lastProductExceedsMaxPrice()) {
            loadAllProducts(productGrid);
        }
    }

    private Double getPrice(WebElement product) {
        List<WebElement> itemPrices = product.findElements(By.className("text-secondary"));
        Double price = null;
        if (!itemPrices.isEmpty()) {
            WebElement priceWithCurrency = itemPrices.get(0);
            List<WebElement> priceElements = priceWithCurrency.findElements(By.tagName("span"));
            if (!priceElements.isEmpty()) {
                price = Double.parseDouble(priceElements.get(0).getText());
            }
        }
        return price;
    }

    private boolean lastProductExceedsMaxPrice() {
        List<WebElement> products = webDriver.findElements(By.className("stock-item-block"));
        WebElement lastProduct = products.get(products.size()-1);
        Double price = getPrice(lastProduct);

        Double maxPrice = 0.0;

        for (SearchCondition searchCondition : this.getSearchConditions()) {
            if (maxPrice < searchCondition.getMaxPrice()) {
                maxPrice = searchCondition.getMaxPrice();
            }
        }

        return price > maxPrice;
    }

}