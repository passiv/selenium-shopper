package sk.johan;

import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import sk.johan.util.SearchResult;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public abstract class AbstractPageComponent {

    protected final WebDriver webDriver;

    private String prefix, url;

    protected AbstractPageTest abstractPageTest;

    @Getter
    @Setter
    private List<SearchCondition> searchConditions;

    public AbstractPageComponent(WebDriver webDriver, String prefix, String url) {
        this.webDriver = webDriver;
        this.prefix = prefix;
        this.url = url;
    }

    public void setAbstractPageTest(AbstractPageTest abstractPageTest) {
        this.abstractPageTest = abstractPageTest;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void navigateTo() {
        this.webDriver.navigate().to(this.prefix + url);
    }

    public <T extends AbstractPageComponent> T initElements(Class<T> classx) {
        T result = PageFactory.initElements(this.webDriver, classx);
        result.setPrefix(this.prefix);
        return result;
    }

    public WebElement findElementById(String id) {
        return webDriver.findElement(By.id(id));
    }

    public WebElement findElementyByXPath(String xpath) {
        return webDriver.findElement(By.xpath(xpath));
    }

    public List<WebElement> findElementsByClass(String className)  {
        return webDriver.findElements(By.className(className));
    }

    public boolean hasPageRendered() {
        WebElement lastElementForTest = findElementById("lastElementForTest");
        return lastElementForTest != null;
    }

    public synchronized List<SearchResult> search(Map<String, SearchResult> allSearchResults) {
        return null;
    }

    public void markElementBeingSearched(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].setAttribute('style', 'background-color: rgba(0,128,0, 0.2) !important;')", element);
    }

    public static synchronized void playSound(final String url) {
        new Thread(new Runnable() {
            // The wrapper thread is unnecessary, unless it blocks on the
            // Clip finishing; see comments.
            public void run() {
                try {
                    File f = new File(url);
                    InputStream targetStream = new FileInputStream(f);
                    InputStream bufferedIn = new BufferedInputStream(targetStream);
                    Clip clip = AudioSystem.getClip();
                    AudioInputStream inputStream = AudioSystem.getAudioInputStream(bufferedIn);

                    clip.open(inputStream);
                    clip.start();
                } catch (Exception e) {
                    throw new RuntimeException("Could not play sound ", e);
                }
            }
        }).start();
    }
}