package sk.johan.shops;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import sk.johan.AbstractPageComponent;
import sk.johan.SearchCondition;
import sk.johan.util.SearchResult;
import sk.johan.util.Util;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LdlcStore extends AbstractPageComponent {
    public LdlcStore(WebDriver webDriver) {
        super(webDriver, "https://www.ldlc.com", "/informatique/pieces-informatique/carte-graphique-interne/c4684/+fdi-1+fp-l310h1845.html");
    }

    @Override
    public synchronized List<SearchResult> search(Map<String, SearchResult> allSearchResults) {
        List<SearchResult> result = new LinkedList<>();
        WebElement productGrid = webDriver.findElement(By.className("listing-product"));

        List<WebElement> productInfos = productGrid.findElements(By.className("dsp-cell-right"));
        for (WebElement productInfo : productInfos) {
            abstractPageTest.fastScrollToElement(productInfo);

            markElementBeingSearched(productInfo);

            WebElement productTitleWrapper = productInfo.findElement(By.className("pdt-desc"));
            WebElement productTitle = productTitleWrapper.findElement(By.tagName("a"));

            WebElement priceElement = productInfo.findElement(By.className("price"));
            if (productTitle != null && priceElement != null) {
                String productTitleText = productTitle.getText();
                String priceText = priceElement.getText().replace("€", ".");
                priceText = priceText.replace(" ", "");

                if (StringUtils.isNotBlank(priceText)) {
                    Double price = Double.parseDouble(priceText);

                    for (SearchCondition searchCondition : this.getSearchConditions()) {
                        if (searchCondition.isMatch(productTitleText, price)) {
                            String searchHitLink = productTitle.getAttribute("href");

                            if (allSearchResults.get(searchHitLink) == null) {
                                SearchResult searchResult = new SearchResult(searchCondition.getSearchPhrase(), price, searchHitLink);
                                result.add(searchResult);
                                System.out.println(Util.formatDateTime(LocalDateTime.now()) + " | " + result.toString());
                                playSound("src/main/resources/notification.wav");
                                Util.sendEmail(searchCondition.getSearchPhrase() + " at " + price, searchResult.toString());
                            }
                        }
                    }
                }
            }
        }
        return result;
    }
}