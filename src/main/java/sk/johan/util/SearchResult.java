package sk.johan.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchResult {

    private String item, link;
    private Double price;

    public SearchResult(String item, Double price, String link) {
        this.item = item;
        this.link = link;
        this.price = price;
    }

    @Override
    public String toString() {
        return  item +
                ", price=" + price +
                ", link=" + link;
    }
}