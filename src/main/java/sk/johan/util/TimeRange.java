package sk.johan.util;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Iterates all LocalDate days between startDate and endDate
 */
public class TimeRange implements Iterable<LocalDateTime> {

    private final LocalDateTime startDate;
    private final LocalDateTime endDate;
    private final Integer secondStep;

    public TimeRange(LocalDateTime startDate, LocalDateTime endDate, Integer secondStep) {
        //check that range is valid (null, start < end)
        this.startDate = startDate;
        this.endDate = endDate;
        this.secondStep = secondStep;
    }

    @Override
    public Iterator<LocalDateTime> iterator() {
        return stream().iterator();
    }

    public Stream<LocalDateTime> stream() {
        return Stream.iterate(startDate, d -> d.plusSeconds(secondStep))
                .limit(getSize());
    }

    public Long getSize() {
        return ChronoUnit.SECONDS.between(startDate, endDate) / secondStep;
    }

    public List<LocalDateTime> toList() { //could also be built from the stream() method
        List<LocalDateTime> dates = new ArrayList<>();
        Iterator<LocalDateTime> iterator = stream().iterator();

        while(iterator.hasNext()) {
            dates.add(iterator.next());
        }
        return dates;
    }
}